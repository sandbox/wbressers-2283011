<?php

namespace Drupal\code_reflection\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles the code_reflection code.
 */
class CodeReflectionController {

  public $value_devider = '|';
  public $types;

  /**
   * Run the constructor.
   */
  public function __construct() {
    $this->types = module_invoke_all('code_reflection_type_info');
  }

  /**
   * Returs the search result page content.
   */
  public function search_results_content() {
    $query = \Drupal::request()->query->get('q');
    $type = explode($this->value_devider, $query);

    // One item found (based on autocomplete).
    if (count($type) == 2) {
      return $this->view_item_content($type[0], $type[1]);
    }

    // Multiple items found.
    $items = array();
    foreach ($this->search($query) as $result) {
      $items[] = $this->view_item_content($result['type'], $result['label'], 'teaser');
    }

    if (count($items) > 0) {
      return array(
        '#theme' => 'item_list',
        '#items' => $items,
        );
    }

    // Nothing found.
    return array(
      '#type' => 'markup',
      '#markup' => t('No items found for <strong>(%query)</strong>.', array('%query' => $query)),
    );
  }

 /**
   * View item title.
   */
  public function view_item_title($type = '', $name = '') {
    $type_info = $this->types[$type];
    return t('@type: @name', array('@type' => $type_info['label'], '@name' => $name));
  }

  /**
   * View full function.
   */
  public function view_item_content($type = '', $name = '', $view_mode = 'full') {
    if ($type_info = $this->types[$type]) {
      if (isset($type_info['theme callback']) && function_exists($type_info['theme callback'])) {
        return call_user_func($type_info['theme callback'], $type, $name, $view_mode);
      }
    }
  }

  /**
   * Retrieves suggestions for block category autocompletion.
   */
  public function autocomplete(Request $request) {
    $query = $request->query->get('q');

    return new JsonResponse($this->search($query));
  }

  /**
   * Simplified searcher.
   */
  protected function search($query) {
    $matches = array();

    foreach ($this->get_options() as $type => $options) {
      foreach ($options as $option) {
        if ($this->search_fuzzy($query, $option)) {
          $matches[] = array(
            'value' => $type . $this->value_devider . $option,
            'label' => $type . ' - ' . $option,
            'type' => $type
          );
        }
      }
    }

    return $matches;
  }

  /**
   * Simplified fuzzy searcher.
   */
  protected function search_fuzzy($needles, $haystack) {
    foreach (explode(' ', $needles) as $needle) {
      if (stripos($haystack, $needle) !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns the type options.
   */
  protected function get_options($type = '') {
    $options = array();

    foreach ($this->types as $key => $type_info) {
      if (isset($type_info['options callback']) && function_exists($type_info['options callback'])) {
        $options[$key] = call_user_func($type_info['options callback']);
      }
    }

    return isset($options[$type]) ? $options[$type] : $options;
  }
}
