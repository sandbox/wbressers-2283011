<?php

/**
 * @file
 * Contains \Drupal\code_reflection\Plugin\Block\SearchBlock.
 */

namespace Drupal\code_reflection\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
use Drupal\code_reflection\Form\SearchForm;

/**
 * Provides a Code reflection search block.
 *
 * @Block(
 *   id = "SearchBlock",
 *   admin_label = @Translation("Code search")
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    return \Drupal::formBuilder()->getForm(new SearchForm());
  }
}
