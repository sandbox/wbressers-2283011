<?php

/**
 * @file
 * Contains \Drupal\code_reflection\Form\SearchForm.
 */

namespace Drupal\code_reflection\Form;

use Drupal\Core\Form\FormBase;

/**
 * Provides a simple example form.
 */
class SearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'code_reflection_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {

    $form['#action'] = $this->url('code_reflection.search_page');
    $form['#token'] = FALSE;
    $form['#method'] = 'get';

    $form['q'] = array(
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'code_reflection.autocomplete',
      '#default_value' => \Drupal::request()->query->get('q'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
  }
}
