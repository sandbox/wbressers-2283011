<?php

/**
 * Themes the code_reflection_function.
 */
function theme_code_reflection_function(array $variables) {
  $output = '';
  $function = $variables['function'];
  $view_mode = $variables['view_mode'];

  $parameters = array_map(function ($parameter) {
    $name = '$' . $parameter->name;
    if ($parameter->isdefaultvalueavailable()) {
      $name .= ' = ' . $parameter->getdefaultvalue();
    }
    return $name;
  } , $function->getParameters());

  if ($view_mode == 'teaser') {
    $title = $function->name . '();';
    $path = 'cr/functions/' . $function->getname();
    $output .='<h2>' . l($title, $path) . '</h2>';
  }
  elseif ($view_mode == 'full') {
    $output .='<h2>Function</h2>' . $function->name . '(' . implode(', ', $parameters). ');';

    // Function is internal.
    if ($function->isInternal()) {
      $path = 'http://nl3.php.net/' . $function->getname();
      $output .='<h2>PHP Url</h2>' . l($function->getname(), $path, array('attributes' => array('target'=>'_blank')));
    }

    // Function is user defined.
    if ($function->isUserDefined()) {
      $filename = $function->getFileName();
      $start_line = $function->getStartLine() - 1;
      $source = file($filename);
      $code = implode("", array_slice($source, $start_line, $function->getEndLine() - $start_line));

      $output .='<h2>Comment</h2><pre>' . $function->getDocComment() . '</pre>';
      $output .='<h2>Code</h2><pre><code>' . $code . '</code></pre>';
    }
  }

  return $output;
}

/**
 * Themes the code_reflection_class.
 */
function theme_code_reflection_class(array $variables) {
  $output = '';
  $class = $variables['class'];
  $view_mode = $variables['view_mode'];

  if ($view_mode == 'teaser') {
    $title = $class->name . '();';
    $path = 'cr/classes/' . $class->getname();
    $output .='<h2>' . l($title, $path) . '</h2>';
  }
  elseif ($view_mode == 'full') {
    $output .='<h2>Function</h2>' . $class->name . '();';

    // Function is internal.
    if ($class->isInternal()) {
      $path = 'http://nl3.php.net/' . $class->getname();
      $output .='<h2>PHP Url</h2>' . l($class->getname(), $path, array('attributes' => array('target'=>'_blank')));
    }

    // Function is user defined.
    if ($class->isUserDefined()) {
      $filename = $class->getFileName();
      $start_line = $class->getStartLine() - 1;
      $source = file($filename);
      $code = implode("", array_slice($source, $start_line, $class->getEndLine() - $start_line));

      $output .='<h2>Comment</h2><pre>' . $class->getDocComment() . '</pre>';
      $output .='<h2>Code</h2><pre><code>' . $code . '</code></pre>';
    }
  }

  return $output;
}

/**
 * Themes the code_reflection_method.
 */
function theme_code_reflection_method(array $variables) {
  $output = 'method goes here';
  $method = $variables['method'];
  $view_mode = $variables['view_mode'];

  // if ($view_mode == 'teaser') {
  //   $title = $method->name . '();';
  //   $path = 'cr/methodes/' . $method->getname();
  //   $output .='<h2>' . l($title, $path) . '</h2>';
  // }
  // elseif ($view_mode == 'full') {
  //   $output .='<h2>Function</h2>' . $method->name . '();';

  //   // Function is internal.
  //   if ($method->isInternal()) {
  //     $path = 'http://nl3.php.net/' . $method->getname();
  //     $output .='<h2>PHP Url</h2>' . l($method->getname(), $path, array('attributes' => array('target'=>'_blank')));
  //   }

  //   // Function is user defined.
  //   if ($method->isUserDefined()) {
  //     $filename = $method->getFileName();
  //     $start_line = $method->getStartLine() - 1;
  //     $source = file($filename);
  //     $code = implode("", array_slice($source, $start_line, $method->getEndLine() - $start_line));

  //     $output .='<h2>Comment</h2><pre>' . $method->getDocComment() . '</pre>';
  //     $output .='<h2>Code</h2><pre><code>' . $code . '</code></pre>';
  //   }
  // }

  return $output;
}
